using System;

namespace Szachy
{
    public static class Utilities
    {
        public static bool NumberIsEven(int num)
        {
            return num % 2 == 0;
        }

        private static bool DoubledAreEqual(double a, double b)
        {
            return Math.Abs(a - b) < 1e-5;
        }

        public static bool DoubledAreNotEqual(double a, double b)
        {
            return !DoubledAreEqual(a, b);
        }

        public enum Direction
        {
            Up,
            UpRight,
            Right,
            DownRight,
            Down,
            DownLeft,
            Left,
            UpLeft
        }
    }
}