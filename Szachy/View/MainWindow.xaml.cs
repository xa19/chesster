﻿using Szachy.ViewModel;

namespace Szachy.View
{
    public partial class MainWindow
    {
        private readonly Game _chessGame;

        public MainWindow()
        {
            InitializeComponent();

            _chessGame = new Game();

            DataContext = _chessGame;
        }
    }
}