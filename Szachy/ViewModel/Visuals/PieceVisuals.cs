﻿using System;
using System.Drawing;
using System.Windows.Media.Imaging;
using Image = System.Windows.Controls.Image;
using Size = System.Windows.Size;

namespace Szachy.ViewModel.Visuals
{
    public class PieceVisuals
    {
        private readonly Bitmap _resourceImage;
        public Image DisplayedImage { get; }

        public PieceVisuals(string resourceImageFilename, Bitmap resourceImage)
        {
            _resourceImage = resourceImage;

            DisplayedImage = new Image
            {
                Source = GetImageFromResources(resourceImageFilename)
            };
        }

        private BitmapImage GetImageFromResources(string fileName)
        {
            return new BitmapImage(new Uri("/Resources/" + fileName, UriKind.Relative));
        }

        public Size GetDisplayedImgSize()
        {
            return new Size(DisplayedImage.Width, DisplayedImage.Height);
        }

        public void AdjustSizeToField(double fieldLength)
        {
            double fractionOfSizeToOccupy = 0.9; // 0.9 = 90% of field size
            double biggerImageSide = Math.Max(_resourceImage.Width, _resourceImage.Height);
            double scaleCooficient = biggerImageSide / (fieldLength * fractionOfSizeToOccupy);

            DisplayedImage.Width = _resourceImage.Width / scaleCooficient;
            DisplayedImage.Height = _resourceImage.Height / scaleCooficient;
        }
    }
}