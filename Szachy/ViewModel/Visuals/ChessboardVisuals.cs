﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using Szachy.Model;

namespace Szachy.ViewModel.Visuals
{
    public class ChessboardVisuals
    {
        public Color FieldsColor1 { get; private set; }
        public Color FieldsColor2 { get; private set; }
        private Rectangle Border { get; set; }

        private double _chessboardEdgeLenghtInPx;

        public double ChessboardEdgeLenghtInPx
        {
            get { return _chessboardEdgeLenghtInPx; }
            set
            {
                if (value > 0 && Utilities.DoubledAreNotEqual(_chessboardEdgeLenghtInPx, value))
                {
                    _chessboardEdgeLenghtInPx = value;
                }
            }
        }

        public ChessboardVisuals(Color fieldColor1, Color fieldColor2)
        {
            SetCheckerColors(fieldColor1, fieldColor2);
        }

        private void SetCheckerColors(Color fieldsColor1, Color fieldsColor2)
        {
            FieldsColor1 = fieldsColor1;
            FieldsColor2 = fieldsColor2;
        }

        public UIElement CustomizeBorder(Color borderColor)
        {
            Border = new Rectangle
            {
                Width = ChessboardEdgeLenghtInPx,
                Height = ChessboardEdgeLenghtInPx,
                Stroke = new SolidColorBrush(borderColor)
            };
            return Border;
        }

        public double GetFieldEdgeLength()
        {
            if (FieldsManager.FieldsPerSide == 0)
            {
                throw new ArgumentException("Fields per side must be greates than zero, otherwise can't calculate edge length for every field");
            }

            return ChessboardEdgeLenghtInPx / FieldsManager.FieldsPerSide;
        }

        public Point GetPositionForFieldAt(int row, int col)
        {
            if (!CoordinateInChessboard.IsWithinChessboardRange(row, col))
            {
                throw new ArgumentOutOfRangeException($"Row position too big - must be in range <0, {FieldsManager.FieldsPerSide - 1}>");
            }

            var positionX = col * GetFieldEdgeLength();
            var positionY = row * GetFieldEdgeLength();

            return new Point(positionX, positionY);
        }
    }
}