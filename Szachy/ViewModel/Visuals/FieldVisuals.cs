﻿using System.Windows;
using System.Windows.Media;
using Point = System.Windows.Point;
using Rectangle = System.Windows.Shapes.Rectangle;

namespace Szachy.ViewModel.Visuals
{
    public class FieldVisuals
    {
        public static readonly Color ChessboardCheckerColor1 = Colors.White;
        public static readonly Color ChessboardCheckerColor2 = Colors.BurlyWood;

        private static readonly Color ChessboardCheckerColor1WhenSelected = Color.FromRgb(230, 255, 221);
        private static readonly Color ChessboardCheckerColor2WhenSelected = Color.FromRgb(210, 255, 190);

        private static readonly Color PiecePositionColor = Colors.Yellow;
        private static readonly Color PossiblePieceBeatColor = Colors.Red;

        private readonly Rectangle _rectangleInForm;
        private readonly Color _originalColor;
        public Point PositionInForm { get; private set; }
        private double EdgeLength => _rectangleInForm.Width;

        public FieldVisuals(double edgeLength, Color color)
        {
            _rectangleInForm = new Rectangle
            {
                Width = edgeLength,
                Height = edgeLength,
                Fill = new SolidColorBrush(color)
            };
            _originalColor = color;
        }

        public void RestoreOriginalColor()
        {
            SetColor(_originalColor);
        }

        public void SelectPiecePosition()
        {
            SetColor(PiecePositionColor);
        }

        public void SelectPossibleMovement()
        {
            if (ChessboardCheckerColor1 == _originalColor)
            {
                SetColor(ChessboardCheckerColor1WhenSelected);
            }
            else if (ChessboardCheckerColor2 == _originalColor)
            {
                SetColor(ChessboardCheckerColor2WhenSelected);
            }
        }

        public void SelectPossibleEnemyBeat()
        {
            SetColor(PossiblePieceBeatColor);
        }

        private void SetColor(Color color)
        {
            _rectangleInForm.Fill = new SolidColorBrush(color);
        }

        public UIElement GetRectangle()
        {
            return _rectangleInForm;
        }

        public void SetPosition(Point position)
        {
            PositionInForm = position;
        }

        private Point GetFieldCenter()
        {
            return PositionInForm + new Vector(EdgeLength / 2, EdgeLength / 2);
        }

        public Point GetPositionForCenteredPieceImage(PieceVisuals pieceImg)
        {
            double halfOfImgWidth = pieceImg.GetDisplayedImgSize().Width / 2;
            double halfOfImgHeight = pieceImg.GetDisplayedImgSize().Height / 2;

            Point positionForImg = GetFieldCenter() - new Vector(halfOfImgWidth, halfOfImgHeight);
            return positionForImg;
        }
    }
}