﻿using System.Collections.ObjectModel;
using Szachy.Model;

namespace Szachy.ViewModel.Visuals
{
    public class RowSymbols : IRowColSymbols
    {
        public ObservableCollection<string> Symbols { get; }

        public RowSymbols()
        {
            Symbols = new ObservableCollection<string>();
            InitializeSymbols();
        }

        public void InitializeSymbols()
        {
            for (int i = 0; i < FieldsManager.FieldsPerSide; i++)
            {
                var invertedIndex = CoordinateConverter.MirrorIndexInRegardToChessboardCenterLines(i - 1);

                string rowMark = invertedIndex.ToString();
                Symbols.Add(rowMark);
            }
        }
    }
}
