﻿using System.Collections.ObjectModel;

namespace Szachy.ViewModel.Visuals
{
    public interface IRowColSymbols
    {
        void InitializeSymbols();
        ObservableCollection<string> Symbols { get; }
    }
}
