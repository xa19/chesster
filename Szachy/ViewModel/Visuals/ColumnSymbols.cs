﻿using System.Collections.ObjectModel;
using Szachy.Model;

namespace Szachy.ViewModel.Visuals
{
    public class ColumnSymbols : IRowColSymbols
    {
        public ObservableCollection<string> Symbols { get; }

        public ColumnSymbols()
        {
            Symbols = new ObservableCollection<string>();
            InitializeSymbols();
        }

        public void InitializeSymbols()
        {
            for (int i = 0; i < FieldsManager.FieldsPerSide; i++)
            {
                string columnMark = CoordinateConverter.NumericIndexToLetter(i).ToString();
                Symbols.Add(columnMark);
            }
        }
    }
}
