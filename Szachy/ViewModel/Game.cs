﻿using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Szachy.Model;
using Szachy.Model.Pieces;
using Szachy.Model.SelectedPiecePossibilitiesAnalysis;
using Szachy.ViewModel.Visuals;

namespace Szachy.ViewModel
{
    [ImplementPropertyChanged]
    public class Game
    {
        private readonly double _chessboardEdgeInPx = 400;

        //-----------------------
        public ObservableCollection<UIElement> ChessboardUiElements { get; }

        private readonly Chessboard _chessboard;
        private readonly ChessboardVisuals _chessboardVisuals;
        private readonly ScenarioManager _scenarioManager;

        private FieldVisuals[,] _fieldVisuals;
        private HashSet<PieceVisuals> _piecesVisuals;

        public RowSymbols RowSymbols { get; } = new RowSymbols();
        public ColumnSymbols ColumnSymbols { get; } = new ColumnSymbols();

        public double FieldEdgeLength => GetFieldEdgeLength();
        public double ChessboardEdgeLenghtInPx => _chessboardVisuals.ChessboardEdgeLenghtInPx;

        public Game(bool initUi = true)
        {
            ChessboardUiElements = new ObservableCollection<UIElement>();

            var fieldColor1 = FieldVisuals.ChessboardCheckerColor1;
            var fieldColor2 = FieldVisuals.ChessboardCheckerColor2;

            _chessboard = new Chessboard();
            _scenarioManager = new ScenarioManager(_chessboard);
            //LoadStandardScenario()
            _scenarioManager.ApplyScenario(new Scenario
            {
                PiecePlacements = delegate
                {
                    PieceFactory.Create<Pawn>(Piece.TeamColor.White, new CoordinateInChessboard("E4"));
                    PieceFactory.Create<Pawn>(Piece.TeamColor.Black, new CoordinateInChessboard("D5"));
                }
            });


            Piece selectedPiece = _chessboard.PieceManager.GetAllPieces().First();
            FieldAnalysisCallerBase.AnalyzeSelectedPiecePossibilities(selectedPiece);

            //_chessboard.FieldManager.DisplayStatusArray();

            //---------------------------------

            if (!initUi)
                return;

            _chessboardVisuals = new ChessboardVisuals(fieldColor1, fieldColor2)
            {
                ChessboardEdgeLenghtInPx = _chessboardEdgeInPx
            };

            InitFieldsApppearance();
            InitChessboardBorderAppearance();
            InitPiecesAppearances();

            ShowPossibilitiesForSelectedPiece();

            RowSymbols = new RowSymbols();
            ColumnSymbols = new ColumnSymbols();
        }

        public Chessboard GetChessboard()
        {
            return _chessboard;
        }

        public void LoadStandardScenario()
        {
            _scenarioManager.ApplyScenario(ScenarioManager.AvailableScenarios.StandardGame);
        }

        private void ShowPossibilitiesForSelectedPiece()
        {
            for (int row = 0; row < Chessboard.StandardFieldsAmount; row++)
            {
                for (int col = 0; col < Chessboard.StandardFieldsAmount; col++)
                {
                    Field field = _chessboard.FieldManager.GetField(row, col);
                    FieldVisuals pieceField = GetFieldAppearance(field.Coords);

                    if (field.Status.IsPossibleMovement())
                    {
                        pieceField.SelectPossibleMovement();
                    }

                    if (field.Status.IsPossibleEnemyBeat())
                    {
                        pieceField.SelectPossibleEnemyBeat();
                    }

                    if (field.Status.IsSelectedPiecePosition())
                    {
                        pieceField.SelectPiecePosition();
                    }
                }
            }
        }

        private FieldVisuals GetFieldAppearance(CoordinateInChessboard coord)
        {
            return _fieldVisuals[coord.Row, coord.Column];
        }

        public void RestoreFieldsOriginalColors()
        {
            foreach (var fieldAppearance in _fieldVisuals)
            {
                fieldAppearance.RestoreOriginalColor();
            }
        }

        private void InitFieldsApppearance()
        {
            var fieldsPerChessboardSide = FieldsManager.FieldsPerSide;
            _fieldVisuals = new FieldVisuals[fieldsPerChessboardSide, fieldsPerChessboardSide];

            for (int row = 0; row < fieldsPerChessboardSide; row++)
            {
                for (int col = 0; col < fieldsPerChessboardSide; col++)
                {
                    var colorForField = GetFieldColorBaseOnPosition(row, col);
                    var fieldAppearance = new FieldVisuals(GetFieldEdgeLength(), colorForField);

                    Point fieldPositionInForm = _chessboardVisuals.GetPositionForFieldAt(row, col);
                    fieldAppearance.SetPosition(fieldPositionInForm);

                    _fieldVisuals[row, col] = fieldAppearance;
                    AddToChessboard(fieldAppearance.GetRectangle(), fieldAppearance.PositionInForm);
                }
            }
        }

        private void InitChessboardBorderAppearance()
        {
            UIElement border = _chessboardVisuals.CustomizeBorder(Colors.Black);
            AddToChessboard(border);
        }

        private void InitPiecesAppearances()
        {
            _piecesVisuals = new HashSet<PieceVisuals>();
            foreach (Piece piece in _chessboard.PieceManager.GetAllPieces())
            {
                bool pieceIsDrawable = piece is IDrawablePiece;
                if (!pieceIsDrawable)
                {
                    throw new Exception($"{piece.GetName()} doesn't implement IDrawablePiece interface");
                }

                IDrawablePiece drawablePiece = (IDrawablePiece)piece;
                PieceVisuals pieceAppearance = piece.IsBlack()
                        ? new PieceVisuals(drawablePiece.BlackImgFilename, drawablePiece.BlackImg)
                        : new PieceVisuals(drawablePiece.WhiteImgFilename, drawablePiece.WhiteImg);

                pieceAppearance.AdjustSizeToField(GetFieldEdgeLength());
                _piecesVisuals.Add(pieceAppearance);

                var piecePositionInForm = GetPositionInFormForPiece(piece, pieceAppearance);
                AddToChessboard(pieceAppearance.DisplayedImage, piecePositionInForm);
            }
        }

        private Point GetPositionInFormForPiece(Piece piece, PieceVisuals pieceAppearance)
        {
            CoordinateInChessboard piecePosition = piece.GetCurrentPosition();
            FieldVisuals fieldAppearance = GetFieldAppearance(piecePosition);
            return fieldAppearance.GetPositionForCenteredPieceImage(pieceAppearance);
        }

        private double GetFieldEdgeLength()
        {
            return _chessboardVisuals.ChessboardEdgeLenghtInPx / FieldsManager.FieldsPerSide;
        }

        private Color GetFieldColorBaseOnPosition(int row, int col)
        {
            return Utilities.NumberIsEven(row + col)
                ? _chessboardVisuals.FieldsColor1
                : _chessboardVisuals.FieldsColor2;
        }

        private void AddToChessboard(UIElement item)
        {
            AddToChessboard(item, new Point(0, 0));
        }

        private void AddToChessboard(UIElement item, Point positionOnCanvas)
        {
            if (!ChessboardUiElements.Contains(item))
            {
                ChessboardUiElements.Add(item);
                SetPositionOnCanvas(item, positionOnCanvas);
            }
        }

        private static void SetPositionOnCanvas(UIElement item, double positionX, double positionY)
        {
            Canvas.SetLeft(item, positionX);
            Canvas.SetTop(item, positionY);
        }

        private static void SetPositionOnCanvas(UIElement item, Point position)
        {
            SetPositionOnCanvas(item, position.X, position.Y);
        }
    }
}