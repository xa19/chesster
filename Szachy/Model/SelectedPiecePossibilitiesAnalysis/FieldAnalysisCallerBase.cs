﻿using System.Collections.Generic;
using System.Linq;
using Szachy.Model.Pieces;
using Szachy.Model.SelectedPiecePossibilitiesAnalysis.FieldCheckingStrategy;

namespace Szachy.Model.SelectedPiecePossibilitiesAnalysis
{
    public abstract class FieldAnalysisCallerBase
    {
        protected CheckingStrategy FieldCheckingStrategy { private get; set; }

        protected FieldStatus AnalyzeFieldStatus(Piece selectedPiece, Field field)
        {
            if (field == null)
            {
                return FieldStatus.GetForIncorrectCoord();
            }

            FieldStatus resultingStatus = FieldCheckingStrategy.GetFieldStatus(field, selectedPiece);
            return resultingStatus;
        }

        protected Field GetRelativeFieldForPiece(Piece piece, CoordinateOffset offsetFromPiecePosition)
        {
            CoordinateInChessboard analyzedFieldCoord = piece.GetRelativeCoordToPiece(offsetFromPiecePosition);
            bool coordOutOfChessboard = !analyzedFieldCoord.IsWithinChessboardRange();

            if (!coordOutOfChessboard)
            {
                return piece.GetRelativeField(offsetFromPiecePosition);
            }

            return null;
        }

        public abstract IEnumerable<Field> AnalyzeField(Piece analyzedPiece);

        public static IEnumerable<Field> AnalyzeSelectedPiecePossibilities(Piece piece)
        {
            var statusOfAllPossibleFields = new List<Field>();

            foreach (FieldAnalysisCallerBase fieldChecker in piece.SelectedPossibleFields)
            {
                IEnumerable<Field> allAnalyzedFields = fieldChecker.AnalyzeField(piece);
                IEnumerable<Field> onlyPossibleFields = allAnalyzedFields.Where(field => field != null);

                statusOfAllPossibleFields.AddRange(onlyPossibleFields);
            }

            return statusOfAllPossibleFields;
        }
    }
}