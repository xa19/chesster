using System;
using System.Collections.Generic;
using Szachy.Model.Pieces;
using Szachy.Model.SelectedPiecePossibilitiesAnalysis.FieldCheckingStrategy;

namespace Szachy.Model.SelectedPiecePossibilitiesAnalysis
{
    public class FieldAnalysisSequentialCaller : FieldAnalysisCallerBase
    {
        public Utilities.Direction Direction { get; }
        public int FieldsAmountLimt { get; }

        public FieldAnalysisSequentialCaller(Utilities.Direction direction, CheckingStrategy checkStrategy, int fieldsAmountLimit = 0)
        {
            FieldCheckingStrategy = checkStrategy;
            Direction = direction;
            FieldsAmountLimt = fieldsAmountLimit;
        }

        public override IEnumerable<Field> AnalyzeField(Piece analyzedPiece)
        {
            var fieldsStatuses = new List<Field>();

            bool fieldLimitHasBeenSet = FieldsAmountLimt > 0;
            int distance = 1;

            do
            {
                CoordinateOffset offsetVector = GetVectorFotGivenDirectionOffset(Direction, distance);
                Field posssibleField = GetRelativeFieldForPiece(analyzedPiece, offsetVector);
                FieldStatus status = AnalyzeFieldStatus(analyzedPiece, posssibleField);

                if (!MovementSequenceCanContinue(status))
                {
                    break;
                }

                fieldsStatuses.Add(posssibleField);

                if (fieldLimitHasBeenSet && distance + 1 > FieldsAmountLimt)
                {
                    break;
                }

                distance++;
            }
            while (true);

            return fieldsStatuses;
        }

        private CoordinateOffset GetVectorFotGivenDirectionOffset(Utilities.Direction direction, int distance)
        {
            int d = distance;
            switch (direction)
            {
                case Utilities.Direction.Up:
                    return new CoordinateOffset(d, 0);
                case Utilities.Direction.Down:
                    return new CoordinateOffset(-d, 0);
                case Utilities.Direction.Left:
                    return new CoordinateOffset(0, -d);
                case Utilities.Direction.Right:
                    return new CoordinateOffset(0, d);
                case Utilities.Direction.UpLeft:
                    return new CoordinateOffset(d, -d);
                case Utilities.Direction.UpRight:
                    return new CoordinateOffset(d, d);
                case Utilities.Direction.DownLeft:
                    return new CoordinateOffset(-d, -d);
                case Utilities.Direction.DownRight:
                    return new CoordinateOffset(-d, d);
                default:
                    throw new ArgumentException("Not possible direction");
            }
        }

        private bool MovementSequenceCanContinue(FieldStatus status)
        {
            var fieldObstacled = 
                status.IsIncorrectCoord() || 
                status.IsPossibleEnemyBeat() || 
                status.IsBlockedByAllyOrEnemy();

            return !fieldObstacled;
        }
    }
}