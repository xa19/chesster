﻿using System.Collections.Generic;
using Szachy.Model.Pieces;
using Szachy.Model.SelectedPiecePossibilitiesAnalysis.FieldCheckingStrategy;

namespace Szachy.Model.SelectedPiecePossibilitiesAnalysis
{
    public class FieldAnalysisSingularCaller : FieldAnalysisCallerBase
    {
        public CoordinateOffset PieceCoordOffset { get; }

        public FieldAnalysisSingularCaller(CoordinateOffset pieceCoordOffset, CheckingStrategy checkStrategy)
        {
            FieldCheckingStrategy = checkStrategy;
            PieceCoordOffset = pieceCoordOffset;
        }

        public FieldAnalysisSingularCaller(CoordinateOffset pieceCoordOffset)
        {
            FieldCheckingStrategy = new CanOnlyBeatEnemy();
            PieceCoordOffset = pieceCoordOffset;
        }

        public override IEnumerable<Field> AnalyzeField(Piece analyzedPiece)
        {
            Field posssibleField = GetRelativeFieldForPiece(analyzedPiece, PieceCoordOffset);
            FieldStatus status = AnalyzeFieldStatus(analyzedPiece, posssibleField);
            return new List<Field> { posssibleField };
        }
    }
}