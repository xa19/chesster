﻿using Szachy.Model.Pieces;
using Status = Szachy.Model.FieldStatus.Status;

namespace Szachy.Model.SelectedPiecePossibilitiesAnalysis.FieldCheckingStrategy
{
    public abstract class CheckingStrategy
    {
        private bool IsCoordCorrect(Field field, Piece analyzedPiece)
        {
            field.Status.ResetFlags();

            if (!field.Coords.IsWithinChessboardRange())
            {
                field.Status.SetAs(Status.CoordIncorrect);
                return false;
            }

            CheckIfFieldIsPiecePosition(analyzedPiece);

            return true;
        }

        private static void CheckIfFieldIsPiecePosition(Piece analyzedPiece)
        {
            FieldStatus analyzedPieceStatus = analyzedPiece.GetField().Status;

            if (!analyzedPieceStatus.IsSelectedPiecePosition())
            {
                analyzedPieceStatus.SetAs(Status.SelectedPiecePosition);
            }
        }


        public FieldStatus GetFieldStatus(Field field, Piece analyzedPiece)
        {
            if (!IsCoordCorrect(field, analyzedPiece))
                return FieldStatus.GetForIncorrectCoord();

            return DetermineFieldStatus(field, analyzedPiece);
        }

        protected abstract FieldStatus DetermineFieldStatus(Field field, Piece analyzedPiece);
    }
}