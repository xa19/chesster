﻿using Szachy.Model.Pieces;
using Status = Szachy.Model.FieldStatus.Status;

namespace Szachy.Model.SelectedPiecePossibilitiesAnalysis.FieldCheckingStrategy
{
    public class CanOnlyBeatEnemy : CheckingStrategy
    {
        protected override FieldStatus DetermineFieldStatus(Field field, Piece analyzedPiece)
        {
            if (field.IsOccupied())
            {
                Piece.TeamColor otherPieceTeamColor = field.GetOccupyingPieceColor();

                if (analyzedPiece.Color != otherPieceTeamColor)
                {
                    field.Status.SetAs(Status.PossibleEnemyBeat);
                }
            }

            return field.Status;
        }
    }
}