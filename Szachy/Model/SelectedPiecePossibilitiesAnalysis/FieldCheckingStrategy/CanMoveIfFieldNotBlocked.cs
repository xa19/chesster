﻿using Szachy.Model.Pieces;
using Status = Szachy.Model.FieldStatus.Status;

namespace Szachy.Model.SelectedPiecePossibilitiesAnalysis.FieldCheckingStrategy
{
    public class CanMoveIfFieldNotBlocked : CheckingStrategy
    {
        protected override FieldStatus DetermineFieldStatus(Field field, Piece analyzedPiece)
        {
            if (field.IsOccupied())
            {
                field.Status.SetAs(Status.AllyOrEnemyThatBlocks);
            }

            field.Status.SetAs(Status.PossibleMove);

            return field.Status;
        }
    }
}