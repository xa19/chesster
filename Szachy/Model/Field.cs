﻿using Szachy.Model.Pieces;

namespace Szachy.Model
{
    public class Field
    {
        public CoordinateInChessboard Coords { get; }

        private Piece _occupyingPiece;

        public FieldStatus Status { get; private set; }

        public Field(int row, int column)
        {
            Coords = new CoordinateInChessboard(row, column);
            Status = new FieldStatus();
        }

        public bool IsOccupied()
        {
            return _occupyingPiece != null;
        }

        public bool IsOccupiedBy(Piece piece)
        {
            return _occupyingPiece == piece;
        }

        public Piece.TeamColor GetOccupyingPieceColor()
        {
            return IsOccupied() ? _occupyingPiece.Color : Piece.TeamColor.None;
        }

        public bool Occupy(Piece piece)
        {
            if (IsOccupied())
            {
                return false;
            }

            _occupyingPiece = piece;
            return true;
        }

        public void Deoccupy()
        {
            _occupyingPiece = null;
        }

        public string GetOccupyingPieceName()
        {
            return _occupyingPiece == null ? string.Empty : _occupyingPiece.GetNameWithColor();
        }
    }
}