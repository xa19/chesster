namespace Szachy.Model
{
    /// <summary>
    /// Used to represent a position of a single chessboard field (and simultaneously 
    /// the chess pieces positions, that could be located in one of the fields)
    /// </summary>
    public class CoordinateInChessboard
    {
        public int Row { get; private set; }        // range: [0, (fieldAmount-1)]
        public int Column { get; private set; }     // range: [0, (fieldAmount-1)]

        public CoordinateInChessboard(int row, int column)
        {
            InitCoords(row, column);
        }

        /// <param name="letterAndDigit">e.g. "D2"</param>
        public CoordinateInChessboard(string letterAndDigit)
        {
            var decodedCoord = CoordinateConverter.GetNumericCoordFromLetterDigitCoord(letterAndDigit);
            InitCoords(decodedCoord.Row, decodedCoord.Column);
        }

        private void InitCoords(int row, int column)
        {
            Row = row;
            Column = column;
        }

        public bool IsWithinChessboardRange()
        {
            return IsInChessboardRange(Row) && IsInChessboardRange(Column);
        }
        public static bool IsWithinChessboardRange(int row, int column)
        {
            return IsInChessboardRange(row) && IsInChessboardRange(column);
        }

        private static bool IsInChessboardRange(int index)
        {
            return 0 <= index && index < FieldsManager.FieldsPerSide;
        }

        public static CoordinateInChessboard operator +(CoordinateInChessboard baseCoord, CoordinateOffset vector)
        {
            int newRow = baseCoord.Row + vector.RowOffset;
            int newColumn = baseCoord.Column + vector.ColumnOffset;

            return new CoordinateInChessboard(newRow, newColumn);
        }
    }
}