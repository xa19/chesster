﻿using System;
using System.Collections.Generic;

namespace Szachy.Model
{
    public class FieldStatus
    {
        public enum Status
        {
            CoordIncorrect,
            PossibleMove,
            PossibleEnemyBeat,
            AllyOrEnemyThatBlocks,
            SelectedPiecePosition
        }

        private int _status;

        public void DescribeCurrent()
        {
            IEnumerable<string> infoToDisplay = new[]
            {
                "This coord for this field is " + (IsIncorrectCoord() ? "OK" : "CORRECT"),
                "The piece " + (IsPossibleMovement() ? "can" : "CAN'T") + " move here",
                "The field " + (IsBlockedByAllyOrEnemy() ? "IS BLOCKED" : "isn't blocked") + " by ally or enemy",
                "The field " + (IsSelectedPiecePosition() ? "IS" : "ins't") + " a piece current position",
                IsPossibleEnemyBeat() ? "This field has no enemy on it" : "There IS AN ENEMY on this field"
            };

            foreach (var info in infoToDisplay)
            {
                Console.WriteLine(info);
            }
        }

        public static FieldStatus GetForIncorrectCoord()
        {
            var status = new FieldStatus();
            status.SetFlag(Status.CoordIncorrect);

            return status;
        }

        public void SetAs(Status status)
        {
            SetFlag(status);
        }

        private void SetFlag(Status status)
        {
            var shift = (int)status;
            _status |= 1 << shift;
        }

        private bool FlagIsSet(Status status)
        {
            var shift = (int)status;
            int flagValue = _status & 1 << shift;
            return flagValue > 0;
        }

        public void ResetFlags()
        {
            _status = 0;
        }

        public bool IsPossibleEnemyBeat()
        {
            return FlagIsSet(Status.PossibleEnemyBeat);
        }

        public bool IsPossibleMovement()
        {
            return FlagIsSet(Status.PossibleMove);
        }

        public bool IsIncorrectCoord()
        {
            return FlagIsSet(Status.CoordIncorrect);
        }

        public bool IsBlockedByAllyOrEnemy()
        {
            return FlagIsSet(Status.AllyOrEnemyThatBlocks);
        }

        public bool IsSelectedPiecePosition()
        {
            return FlagIsSet(Status.SelectedPiecePosition);
        }
    }
}