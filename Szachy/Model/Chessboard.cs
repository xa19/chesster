﻿using System;

namespace Szachy.Model
{
    public class Chessboard
    {
        public static readonly int StandardFieldsAmount = 8;

        public PieceManager PieceManager { get; private set; }
        public FieldsManager FieldManager { get; private set; }

        public Chessboard()
        {
            if (StandardFieldsAmount <= 1)
            {
                throw new Exception("Chessboard fields amount per side must be greater than 1");
            }

            PieceManager = new PieceManager();
            FieldManager = new FieldsManager();
            PieceFactory.SetChessboard(this);
        }
    }
}