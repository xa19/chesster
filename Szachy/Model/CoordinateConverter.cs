using System;
using System.Text.RegularExpressions;

namespace Szachy.Model
{
    /// <summary>
    /// Used to convert the letter-digit coordinate format (e.g. B3) to numeric coordinates form
    /// </summary>
    public static class CoordinateConverter
    {
        /// <param name="letterAndDigit">ChessboardCoordinate in format e.g. C3</param>
        public static CoordinateInChessboard GetNumericCoordFromLetterDigitCoord(string letterAndDigit)
        {
            letterAndDigit = letterAndDigit.ToUpper();

            if (!IsCorrectLetterDigitCoord(letterAndDigit))
            {
                throw new ArgumentException("The field is not in the right chess position format. Correct format: e.g. B3");
            }

            char letter = letterAndDigit[0];
            char digit = letterAndDigit[1];

            int column = LetterToNumericIndex(letter);
            var row = RowNumberToRowIndex(digit);

            /* In this program the chessboard is an 2D array, so A1 = [0,0] would be treated as position in
             * upper left corner, but in real chessboard symbols start from the bottom left corner. 
             * The column index is fine, we just need to invert the row index to get the real chessboard position 
             * A1 = [0,0] in real chessboard = [7,0] internally, in array
             */
            int mirroredIndex = MirrorIndexInRegardToChessboardCenterLines(row);

            return new CoordinateInChessboard(mirroredIndex, column);
        }

        private static bool IsCorrectLetterDigitCoord(string letterAndDigit)
        {
            char maxColumnLetter = NumericIndexToLetter(Chessboard.StandardFieldsAmount);
            string maxRowNumber = Chessboard.StandardFieldsAmount.ToString();

            Regex fieldRegex = new Regex($"[A-{maxColumnLetter}][1-{maxRowNumber}]");
            return fieldRegex.IsMatch(letterAndDigit.ToUpper());
        }

        // e.g. for 8x8 chessboard: 0 -> 7, 1 -> 6, 3 -> 4, 4 -> 3
        public static int MirrorIndexInRegardToChessboardCenterLines(int rowOrColumnIndex)
        {
            return FieldsManager.FieldsPerSide - 1 - rowOrColumnIndex;
        }

        private static int LetterToNumericIndex(char letter)
        {
            // Use the fact that the next alphabetic letters in the ASCII are next to each other
            return GetAsciiCodeFor(letter) - GetAsciiCodeFor('A');
        }

        public static char NumericIndexToLetter(int columnIndex)
        {
            int columnIndexToConvert = GetAsciiCodeFor('A') + columnIndex;
            return (char) columnIndexToConvert;
        }

        private static int GetAsciiCodeFor(char letter)
        {
            return letter;
        }

        // e.g. '3' -> 2, '1' -> 0
        private static int RowNumberToRowIndex(char rowNumberAsChar)
        {
            int asNumber = int.Parse(rowNumberAsChar.ToString());
            int rowIndex = asNumber - 1;    // to array index [0,7] (in standard chessboard)
            return rowIndex;   
        }
    }
}