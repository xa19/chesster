﻿namespace Szachy.Model
{
    /// <summary>
    /// Class that represents vertical and horizontal shifts from the chosen coordinate
    /// e.g. coord[1, 3] + offset[3, 4] = [1+3, 3+4] = [4, 7].
    /// Used to, among others, hardcode chess pieces possible moves.
    /// </summary>
    public class CoordinateOffset
    {
        public int RowOffset { get;  }
        public int ColumnOffset { get; }

        public CoordinateOffset(int rowOffset, int columnOffset)
        {
            // Array starts from the top (towards bottom), but chess rows from the bottom (towards top)
            // So to go lower, we need to subtract the offset (invert its direction)
            RowOffset = InvertOffsetDirection(rowOffset);
            ColumnOffset = columnOffset;
        }

        private int InvertOffsetDirection(int offset)
        {
            return -offset;
        }
    }
}