using System;
using Szachy.Model.ChessboardElements;
using Szachy.Model.ChessPieces;

namespace Szachy.Model
{
    public class PossibilitiesAnalyzer
    {
        private ChessPiece _piece;

        public void ReflectPossibilitiesInFields(ChessPiece piece)
        {
            _piece = piece;

            CheckFieldWithSelectedChessPiece();

            if (_piece is King)
            {
                // Diagonally
                CheckFieldStatus(new CoordVector(1, 1), IfCantMoveCanBeat);
                CheckFieldStatus(new CoordVector(1, 1), IfCantMoveCanBeat);
                CheckFieldStatus(new CoordVector(-1, 1), IfCantMoveCanBeat);
                CheckFieldStatus(new CoordVector(1, -1), IfCantMoveCanBeat);
                CheckFieldStatus(new CoordVector(-1, -1), IfCantMoveCanBeat);

                // Up / down / left / right
                CheckFieldStatus(new CoordVector(1, 0), IfCantMoveCanBeat);
                CheckFieldStatus(new CoordVector(-1, 0), IfCantMoveCanBeat);
                CheckFieldStatus(new CoordVector(0, -1), IfCantMoveCanBeat);
                CheckFieldStatus(new CoordVector(0, 1), IfCantMoveCanBeat);
            }
            else if (_piece is Bishop)
            {
                CheckSequenceOfFields(Direction.UpLeft, IfCantMoveCanBeat);
                CheckSequenceOfFields(Direction.UpRight, IfCantMoveCanBeat);
                CheckSequenceOfFields(Direction.DownLeft, IfCantMoveCanBeat);
                CheckSequenceOfFields(Direction.DownRight, IfCantMoveCanBeat);
            }
            else if (_piece is Castel)
            {
                CheckSequenceOfFields(Direction.Up, IfCantMoveCanBeat);
                CheckSequenceOfFields(Direction.Down, IfCantMoveCanBeat);
                CheckSequenceOfFields(Direction.Left, IfCantMoveCanBeat);
                CheckSequenceOfFields(Direction.Right, IfCantMoveCanBeat);
            }
            else if (_piece is Knight)
            {
                CheckFieldStatus(new CoordVector(1, 2), IfCantMoveCanBeat);
                CheckFieldStatus(new CoordVector(2, 1), IfCantMoveCanBeat);

                CheckFieldStatus(new CoordVector(-1, 2), IfCantMoveCanBeat);
                CheckFieldStatus(new CoordVector(-2, 1), IfCantMoveCanBeat);

                CheckFieldStatus(new CoordVector(-1, -2), IfCantMoveCanBeat);
                CheckFieldStatus(new CoordVector(-2, -1), IfCantMoveCanBeat);

                CheckFieldStatus(new CoordVector(1, -2), IfCantMoveCanBeat);
                CheckFieldStatus(new CoordVector(2, -1), IfCantMoveCanBeat);
            }
            else if (_piece is Pawn)
            {
                // Only moves:
                uint howManyFieldsToCheck = _piece.AlreadyDidHisFirstMove() ? (uint)1 : 2;
                Direction checkDirection = _piece.IsWhite() ? Direction.Up : Direction.Down;
                CheckSequenceOfFields(checkDirection, howManyFieldsToCheck, IfCantMoveDoNothing);

                // Only enemy pieces beat
                if (_piece.IsWhite())
                {
                    CheckFieldStatus(new CoordVector(1, -1), CanBeatIfEnemyOnField);
                    CheckFieldStatus(new CoordVector(1, 1), CanBeatIfEnemyOnField);
                }
                else if (_piece.IsBlack())
                {
                    CheckFieldStatus(new CoordVector(-1, -1), CanBeatIfEnemyOnField);
                    CheckFieldStatus(new CoordVector(-1, 1), CanBeatIfEnemyOnField);
                }
            }
            else if (_piece is Queen)
            {
                CheckSequenceOfFields(Direction.UpLeft, IfCantMoveCanBeat);
                CheckSequenceOfFields(Direction.UpRight, IfCantMoveCanBeat);
                CheckSequenceOfFields(Direction.DownLeft, IfCantMoveCanBeat);
                CheckSequenceOfFields(Direction.DownRight, IfCantMoveCanBeat);

                CheckSequenceOfFields(Direction.Up, IfCantMoveCanBeat);
                CheckSequenceOfFields(Direction.Down, IfCantMoveCanBeat);
                CheckSequenceOfFields(Direction.Left, IfCantMoveCanBeat);
                CheckSequenceOfFields(Direction.Right, IfCantMoveCanBeat);
            }
            else
            {
                throw new ArgumentException("Chess piece movements not defined");
            }
        }

        public enum ChessFieldStatus
        {
            None,
            CoordInvalid,
            PossibleMovement,
            PossibleEnemyChessPieceBeat,
            AllyOrEnemyThatBlocks
        }

        private enum Direction
        {
            Up,
            UpRight,
            Right,
            DownRight,
            Down,
            DownLeft,
            Left,
            UpLeft
        }

        private void CheckSequenceOfFields(Direction offsetDirection, uint visitedFieldsLimit, Func<ChessField, ChessFieldStatus> checkingMethod)
        {
            for (int i = 1; i <= visitedFieldsLimit; i++)
            {
                CoordVector offsetVector;
                switch (offsetDirection)
                {
                    case Direction.Up:
                        offsetVector = new CoordVector(i, 0);
                        break;
                    case Direction.Down:
                        offsetVector = new CoordVector(-i, 0);
                        break;
                    case Direction.Left:
                        offsetVector = new CoordVector(0, -i);
                        break;
                    case Direction.Right:
                        offsetVector = new CoordVector(0, i);
                        break;
                    case Direction.UpLeft:
                        offsetVector = new CoordVector(i, -i);
                        break;
                    case Direction.UpRight:
                        offsetVector = new CoordVector(i, i);
                        break;
                    case Direction.DownLeft:
                        offsetVector = new CoordVector(-i, -i);
                        break;
                    case Direction.DownRight:
                        offsetVector = new CoordVector(-i, i);
                        break;
                    default:
                        throw new ArgumentException("Not possible direction");
                }

                ChessFieldStatus status = CheckFieldStatus(offsetVector, checkingMethod);

                if (!MovementSequenceCanContinue(status))
                {
                    break;
                }
            }
        }

        private void CheckSequenceOfFields(Direction offsetDirection, Func<ChessField, ChessFieldStatus> checkingMethod)
        {
            CheckSequenceOfFields(offsetDirection, Chessboard.StandardFieldsAmount - 1, checkingMethod);
        }

        private bool MovementSequenceCanContinue(ChessFieldStatus status)
        {
            var fieldObstacled = status == ChessFieldStatus.PossibleEnemyChessPieceBeat || status == ChessFieldStatus.AllyOrEnemyThatBlocks;

            return !fieldObstacled;
        }

        public ChessFieldStatus CheckFieldStatus(CoordVector fieldOffset)
        {
            return CheckFieldStatus(fieldOffset, IfCantMoveCanBeat);
        }

        public ChessFieldStatus CheckFieldStatus(CoordVector fieldOffset, Func<ChessField, ChessFieldStatus> checkingMethod)
        {
            Coordinate coords = _piece.GetCurrentPosition() + fieldOffset;

            if (!coords.IsValidChessCoord)
            {
                return ChessFieldStatus.CoordInvalid;
            }

            ChessField field = _piece.GetField(coords);
            field.ResetFlags();

            var x = field.GetOccupyingChessPieceName();

            if (field.IsOccupiedBy(_piece))
            {
                field.IsSelectedChessPiecePosition = true;
            }

            return checkingMethod.Invoke(field);
        }

        protected ChessFieldStatus IfCantMoveCanBeat(ChessField field)
        {
            if (field.IsOccupied())
            {
                if (field.IsOccupiedByEnemyFor(_piece))
                {
                    field.PossibleEnemyBeat = true;
                    return ChessFieldStatus.PossibleEnemyChessPieceBeat;
                }

                field.PossibleMove = false;
                return ChessFieldStatus.AllyOrEnemyThatBlocks;
            }

            field.PossibleMove = true;
            return ChessFieldStatus.PossibleMovement;
        }

        protected ChessFieldStatus IfCantMoveDoNothing(ChessField field)
        {
            if (field.IsOccupied())
            {
                field.PossibleMove = false;
                return ChessFieldStatus.AllyOrEnemyThatBlocks;
            }

            field.PossibleMove = true;
            return ChessFieldStatus.PossibleMovement;
        }

        protected ChessFieldStatus CanBeatIfEnemyOnField(ChessField field)
        {
            if (field.IsOccupiedByEnemyFor(_piece))
            {
                field.PossibleEnemyBeat = true;
                return ChessFieldStatus.PossibleEnemyChessPieceBeat;
            }

            return ChessFieldStatus.None;
        }

        private void CheckFieldWithSelectedChessPiece()
        {
            CheckFieldStatus(new CoordVector(0, 0));
        }

    }
}