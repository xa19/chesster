﻿using System;
using System.Collections.Generic;
using System.Data;
using Szachy.Model.Pieces;

namespace Szachy.Model
{
    public class ScenarioManager
    {
        private readonly Chessboard _chessboard;
        private Scenario _currentScenario;

        // Possible chess scenarios that can be chosen from the Chessboard class

        public ScenarioManager(Chessboard chessboard)
        {
            _chessboard = chessboard;

            if (_chessboard.FieldManager.ThereAreUninitializedFields())
            {
                throw new NoNullAllowedException("There are uninitialized chessboard fields");
            }
        }

        private void ResetChessboard()
        {
            PieceManager.DeoccupyAllFields(_chessboard);
            _chessboard.PieceManager.ClearPieces();
        }

        public void ApplyScenario(AvailableScenarios availableScenarioName)
        {
            ResetChessboard();
            if (!ScenarioIsChosable(availableScenarioName))
            {
                throw new ArgumentException("This scenario has not been set");
            }

            _currentScenario = _chosableScenarios[availableScenarioName];
            ApplyScenario(_currentScenario);
        }

        public void ApplyScenario(Scenario scenario)
        {
            ResetChessboard();

            _currentScenario = scenario;
            if (!ThereAreRequiredFieldsAmount())
            {
                throw new ArgumentException("The chessboard doesn't have required fields amount per side. Don't know how to put the chess pieces.");
            }

            _currentScenario.Apply();
        }

        private bool ScenarioIsChosable(AvailableScenarios availableScenarioName)
        {
            return _chosableScenarios.ContainsKey(availableScenarioName);
        }

        private bool ThereAreRequiredFieldsAmount()
        {
            if (_currentScenario == null)
            {
                throw new NoNullAllowedException("Current scenario has not been initialized");
            }

            var currentFieldsAmount = _currentScenario.RequiredFieldsAmountPerSide;
            return FieldsManager.FieldsPerSide == currentFieldsAmount;
        }

        public enum AvailableScenarios
        {
            StandardGame,
            KingInTheChessboardTests,
            BishopInTheChessboardTests,
            PawnInTheChessboardTests,
            CastelInTheChessboardTests,
            KnightInTheChessboardTests,
            QueenInTheChessboardTests,
        }

        private static readonly CoordinateInChessboard CommonCenterCoord = new CoordinateInChessboard("F5");
        private readonly Dictionary<AvailableScenarios, Scenario> _chosableScenarios = new Dictionary<AvailableScenarios, Scenario>
        {
            [AvailableScenarios.StandardGame] = new Scenario
            {
                PiecePlacements = delegate
                {
                    PieceFactory.Create<King>(Piece.TeamColor.White, new CoordinateInChessboard("E1"));
                    PieceFactory.Create<King>(Piece.TeamColor.Black, new CoordinateInChessboard("E8"));

                    PieceFactory.Create<Bishop>(Piece.TeamColor.White, new CoordinateInChessboard("C1"));
                    PieceFactory.Create<Bishop>(Piece.TeamColor.White, new CoordinateInChessboard("F1"));
                    PieceFactory.Create<Bishop>(Piece.TeamColor.Black, new CoordinateInChessboard("C8"));
                    PieceFactory.Create<Bishop>(Piece.TeamColor.Black, new CoordinateInChessboard("F8"));

                    PieceFactory.Create<Queen>(Piece.TeamColor.White, new CoordinateInChessboard("D1"));
                    PieceFactory.Create<Queen>(Piece.TeamColor.Black, new CoordinateInChessboard("D8"));

                    PieceFactory.Create<Castel>(Piece.TeamColor.White, new CoordinateInChessboard("A1"));
                    PieceFactory.Create<Castel>(Piece.TeamColor.White, new CoordinateInChessboard("H1"));
                    PieceFactory.Create<Castel>(Piece.TeamColor.Black, new CoordinateInChessboard("A8"));
                    PieceFactory.Create<Castel>(Piece.TeamColor.Black, new CoordinateInChessboard("H8"));

                    PieceFactory.Create<Knight>(Piece.TeamColor.White, new CoordinateInChessboard("B1"));
                    PieceFactory.Create<Knight>(Piece.TeamColor.White, new CoordinateInChessboard("G1"));
                    PieceFactory.Create<Knight>(Piece.TeamColor.Black, new CoordinateInChessboard("B8"));
                    PieceFactory.Create<Knight>(Piece.TeamColor.Black, new CoordinateInChessboard("G8"));

                    for (int i = 0; i <= 7; i++)
                    {
                        PieceFactory.Create<Pawn>(Piece.TeamColor.White, new CoordinateInChessboard(column: i, row: 6));
                        PieceFactory.Create<Pawn>(Piece.TeamColor.Black, new CoordinateInChessboard(column: i, row: 1));
                    }
                }
            },
            [AvailableScenarios.KingInTheChessboardTests] = new Scenario
            {
                PiecePlacements = delegate
                {
                    PieceFactory.Create<King>(Piece.TeamColor.White, CommonCenterCoord);
                }
            },
            [AvailableScenarios.BishopInTheChessboardTests] = new Scenario
            {
                PiecePlacements = delegate
                {
                    PieceFactory.Create<Bishop>(Piece.TeamColor.White, CommonCenterCoord);
                }
            },
            [AvailableScenarios.PawnInTheChessboardTests] = new Scenario
            {
                PiecePlacements = delegate
                {
                    PieceFactory.Create<Pawn>(Piece.TeamColor.White, CommonCenterCoord);
                }
            },
            [AvailableScenarios.CastelInTheChessboardTests] = new Scenario
            {
                PiecePlacements = delegate
                {
                    PieceFactory.Create<Castel>(Piece.TeamColor.White, CommonCenterCoord);
                }
            },
            [AvailableScenarios.KnightInTheChessboardTests] = new Scenario
            {
                PiecePlacements = delegate
                {
                    PieceFactory.Create<Knight>(Piece.TeamColor.White, CommonCenterCoord);
                }
            },
            [AvailableScenarios.QueenInTheChessboardTests] = new Scenario
            {
                PiecePlacements = delegate
                {
                    PieceFactory.Create<Queen>(Piece.TeamColor.White, CommonCenterCoord);
                }
            }
        };
    }
}