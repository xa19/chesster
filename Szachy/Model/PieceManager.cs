﻿using System;
using System.Collections.Generic;
using System.Linq;
using Szachy.Model.Pieces;

namespace Szachy.Model
{
    public class PieceManager
    {
        public readonly List<Piece> Pieces;

        public PieceManager()
        {
            Pieces = new List<Piece>();
        }

        public IEnumerable<Piece> GetAllPieces()
        {
            return Pieces;
        }

        public IEnumerable<TPiece> GetAllPiecesOfSpecifiedType<TPiece>()
        {
            return Pieces.OfType<TPiece>().Select(piece => (TPiece)Convert.ChangeType(piece, typeof(TPiece)));
        }

        public IEnumerable<Piece> GetAllPiecesOfSpecifiedColor(Piece.TeamColor color)
        {
            return Pieces.Where(piece => piece.GetColor() == color);
        }

        public void ClearPieces()
        {
            Pieces.Clear();
        }

        public static void DeoccupyAllFields(Chessboard chessboard)
        {
            foreach (var field in chessboard.FieldManager.GetAllFields())
            {
                field.Deoccupy();
            }
        }
    }
}
