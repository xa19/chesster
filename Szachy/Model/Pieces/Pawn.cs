﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Szachy.Model.SelectedPiecePossibilitiesAnalysis;
using Szachy.Model.SelectedPiecePossibilitiesAnalysis.FieldCheckingStrategy;
using Direction = Szachy.Utilities.Direction;

namespace Szachy.Model.Pieces
{
    public class Pawn : Piece, IDrawablePiece
    {
        // TODO Refactor

        public string WhiteImgFilename { get; } = "white_pawn.gif";
        public string BlackImgFilename { get; } = "black_pawn.gif";
        public Bitmap WhiteImg { get; } = Properties.Resources.white_pawn;
        public Bitmap BlackImg { get; } = Properties.Resources.black_pawn;

        public Pawn(TeamColor teamColor, Chessboard chessboard) : base(teamColor, chessboard)
        {
            SelectedPossibleFields = new List<FieldAnalysisCallerBase>();

            // Only moves:
            int howManyFieldsToCheck = AlreadyDidHisFirstMove() ? 1 : 2;
            Direction checkDirection = IsWhite() ? Direction.Up : Direction.Down;
            CheckingStrategy fieldChecker = new CanMoveIfFieldNotBlocked();
            SelectedPossibleFields.Add(new FieldAnalysisSequentialCaller(checkDirection, fieldChecker, howManyFieldsToCheck));

            // Only enemy pieces beat
            CoordinateOffset possibleEnemyLeft, possibleEnemyRight;

            if (IsWhite())
            {
                possibleEnemyLeft = new CoordinateOffset(1, -1);
                possibleEnemyRight = new CoordinateOffset(1, 1);
            }
            else if (IsBlack())
            {
                possibleEnemyLeft = new CoordinateOffset(-1, -1);
                possibleEnemyRight = new CoordinateOffset(-1, 1);
            }
            else
            {
                throw new Exception("Forbidden team color");
            }

            SelectedPossibleFields.Add(new FieldAnalysisSingularCaller(possibleEnemyLeft, new CanOnlyBeatEnemy()));
            SelectedPossibleFields.Add(new FieldAnalysisSingularCaller(possibleEnemyRight, new CanOnlyBeatEnemy()));
        }
    }
}