﻿using System.Collections.Generic;
using System.Drawing;
using Szachy.Model.SelectedPiecePossibilitiesAnalysis;
using Szachy.Model.SelectedPiecePossibilitiesAnalysis.FieldCheckingStrategy;
using Direction = Szachy.Utilities.Direction;

namespace Szachy.Model.Pieces
{
    public class Castel : Piece, IDrawablePiece
    {
        public string WhiteImgFilename { get; } = "white_castel.gif";
        public string BlackImgFilename { get; } = "black_castel.gif";
        public Bitmap WhiteImg { get; } = Properties.Resources.white_castel;
        public Bitmap BlackImg { get; } = Properties.Resources.black_castel;

        public Castel(TeamColor teamColor, Chessboard chessboard) : base(teamColor, chessboard)
        {
            SelectedPossibleFields = new List<FieldAnalysisCallerBase>
            {
                new FieldAnalysisSequentialCaller(Direction.Up, new CanMoveAndBeatEnemy()),
                new FieldAnalysisSequentialCaller(Direction.Down, new CanMoveAndBeatEnemy()),
                new FieldAnalysisSequentialCaller(Direction.Left, new CanMoveAndBeatEnemy()),
                new FieldAnalysisSequentialCaller(Direction.Right, new CanMoveAndBeatEnemy()),
            };
        }
    }
}