﻿using System.Collections.Generic;
using System.Drawing;
using Szachy.Model.SelectedPiecePossibilitiesAnalysis;
using Szachy.Model.SelectedPiecePossibilitiesAnalysis.FieldCheckingStrategy;

namespace Szachy.Model.Pieces
{
    public class King : Piece, IDrawablePiece
    {
        public string WhiteImgFilename { get; } = "white_king.gif";
        public string BlackImgFilename { get; } = "black_king.gif";
        public Bitmap WhiteImg { get; } = Properties.Resources.white_king;
        public Bitmap BlackImg { get; } = Properties.Resources.black_king;

        public King(TeamColor teamColor, Chessboard chessboard) : base(teamColor, chessboard)
        {
            SelectedPossibleFields = new List<FieldAnalysisCallerBase>
            {
                // Diagonally
                new FieldAnalysisSingularCaller(new CoordinateOffset(-1, -1), new CanMoveAndBeatEnemy()),
                new FieldAnalysisSingularCaller(new CoordinateOffset(1, -1), new CanMoveAndBeatEnemy()),
                new FieldAnalysisSingularCaller(new CoordinateOffset(-1, 1), new CanMoveAndBeatEnemy()),
                new FieldAnalysisSingularCaller(new CoordinateOffset(1, 1), new CanMoveAndBeatEnemy()),

                // Up / down / left / right
                new FieldAnalysisSingularCaller(new CoordinateOffset(1, 0), new CanMoveAndBeatEnemy()),
                new FieldAnalysisSingularCaller(new CoordinateOffset(-1, 0), new CanMoveAndBeatEnemy()),
                new FieldAnalysisSingularCaller(new CoordinateOffset(0, -1), new CanMoveAndBeatEnemy()),
                new FieldAnalysisSingularCaller(new CoordinateOffset(0, 1), new CanMoveAndBeatEnemy()),
            };
        }
    }
}