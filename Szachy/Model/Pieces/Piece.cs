﻿using System;
using System.Collections.Generic;
using System.Linq;
using Szachy.Model.SelectedPiecePossibilitiesAnalysis;

namespace Szachy.Model.Pieces
{
    public abstract class Piece
    {
        // TODO Castling (King and Castel movement)
        // TODO Pawn type change when got to the row on the other side of chessboard

        private readonly Chessboard _chessboard;
        private uint _movesCounter;

        private List<FieldAnalysisCallerBase> _selectedPossibleFields;
        public List<FieldAnalysisCallerBase> SelectedPossibleFields
        {
            get
            {
                if (_selectedPossibleFields == null)
                {
                    throw new Exception("Possible chess piece actions has not been set for " + GetNameWithColor());
                }

                return _selectedPossibleFields;
            }
            protected set { _selectedPossibleFields = value; }
        }

        public enum TeamColor
        {
            None, White, Black
        }

        public TeamColor Color { get; }

        protected Piece(TeamColor teamColor, Chessboard chessboard)
        {
            Color = teamColor;
            _chessboard = chessboard;
            _movesCounter = 0;
        }

        public void PlaceOn(CoordinateInChessboard coord, bool isBeingPlaceOnStartingPosition = false)
        {
            _chessboard.FieldManager.DeoccupyFieldOnWhichThePieceIs(this);

            Field fieldToPutAt = _chessboard.FieldManager.GetField(coord);
            bool placementAccepted = fieldToPutAt.Occupy(this);

            if (!placementAccepted)
            {
                var occupyingPiece = fieldToPutAt.GetOccupyingPieceName();
                throw new Exception($"Field already occupied by {occupyingPiece}");
            }

            if(!isBeingPlaceOnStartingPosition)
            {
                _movesCounter++;
            }
        }

        public string GetName()
        {
            string fullClassPath = GetType().ToString();    // e.g. Szachy.Model.Pieces.King

            if (!fullClassPath.Contains('.'))
                return fullClassPath;

            return fullClassPath.Split('.').Last();
        }

        public string GetNameWithColor()
        {
            string colorName = IsBlack() ? "Black" : "White";
            return $"{colorName} {GetName()}";
        }

        public CoordinateInChessboard GetCurrentPosition()
        {
            CoordinateInChessboard currentPosition = _chessboard.FieldManager.GetPositionOfPiece(this);

            if (currentPosition == null)
            {
                throw new NullReferenceException("Piece isn't on a chessboard");
            }

            return currentPosition;
        }

        public CoordinateInChessboard GetRelativeCoordToPiece(CoordinateOffset fieldOffset)
        {
            return GetCurrentPosition() + fieldOffset;
        }

        public Field GetRelativeField(CoordinateOffset fieldOffset)
        {
            CoordinateInChessboard coords = GetRelativeCoordToPiece(fieldOffset);
            return _chessboard.FieldManager.GetField(coords);
        }

        public Field GetField()
        {
            CoordinateInChessboard coords = GetCurrentPosition();
            return _chessboard.FieldManager.GetField(coords);
        }

        public void SetMovesCounter(uint count)
        {
            _movesCounter = count;
        }

        public TeamColor GetColor()
        {
            return Color;

        }

        public bool IsBlack()
        {
            return Color == TeamColor.Black;
        }

        public bool IsWhite()
        {
            return Color == TeamColor.White;
        }

        protected bool AlreadyDidHisFirstMove()
        {
            return _movesCounter > 0;
        }

        public bool CanBeatPiece(Piece piece)
        {
            FieldAnalysisCallerBase.AnalyzeSelectedPiecePossibilities(this);

            Field enemyField = _chessboard.FieldManager.GetFieldOnWhichThePieceIs(piece);
            return enemyField.Status.IsPossibleEnemyBeat();
        }
    }
}