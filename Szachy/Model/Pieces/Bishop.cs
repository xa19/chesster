﻿using System.Collections.Generic;
using System.Drawing;
using Szachy.Model.SelectedPiecePossibilitiesAnalysis;
using Szachy.Model.SelectedPiecePossibilitiesAnalysis.FieldCheckingStrategy;
using Direction = Szachy.Utilities.Direction;

namespace Szachy.Model.Pieces
{
    public class Bishop : Piece, IDrawablePiece
    {
        public string WhiteImgFilename { get; } = "white_bishop.gif";
        public string BlackImgFilename { get; } = "black_bishop.gif";
        public Bitmap WhiteImg { get; } = Properties.Resources.white_bishop;
        public Bitmap BlackImg { get; } = Properties.Resources.black_bishop;

        public Bishop(TeamColor teamColor, Chessboard chessboard) : base(teamColor, chessboard)
        {
            SelectedPossibleFields = new List<FieldAnalysisCallerBase>
            {
                new FieldAnalysisSequentialCaller(Direction.UpLeft, new CanMoveAndBeatEnemy()),
                new FieldAnalysisSequentialCaller(Direction.UpRight, new CanMoveAndBeatEnemy()),
                new FieldAnalysisSequentialCaller(Direction.DownLeft, new CanMoveAndBeatEnemy()),
                new FieldAnalysisSequentialCaller(Direction.DownRight, new CanMoveAndBeatEnemy()),
            };
        }
    }
}