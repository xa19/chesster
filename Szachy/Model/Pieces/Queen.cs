﻿using System.Collections.Generic;
using System.Drawing;
using Szachy.Model.SelectedPiecePossibilitiesAnalysis;
using Szachy.Model.SelectedPiecePossibilitiesAnalysis.FieldCheckingStrategy;
using Direction = Szachy.Utilities.Direction;

namespace Szachy.Model.Pieces
{
    public class Queen : Piece, IDrawablePiece
    {
        public string WhiteImgFilename { get; } = "white_queen.gif";
        public string BlackImgFilename { get; } = "black_queen.gif";
        public Bitmap WhiteImg { get; } = Properties.Resources.white_queen;
        public Bitmap BlackImg { get; } = Properties.Resources.black_queen;

        public Queen(TeamColor teamColor, Chessboard chessboard) : base(teamColor, chessboard)
        {
            SelectedPossibleFields = new List<FieldAnalysisCallerBase>
            {
                new FieldAnalysisSequentialCaller(Direction.UpLeft, new CanMoveAndBeatEnemy()),
                new FieldAnalysisSequentialCaller(Direction.UpRight, new CanMoveAndBeatEnemy()),
                new FieldAnalysisSequentialCaller(Direction.DownLeft, new CanMoveAndBeatEnemy()),
                new FieldAnalysisSequentialCaller(Direction.DownRight, new CanMoveAndBeatEnemy()),

                new FieldAnalysisSequentialCaller(Direction.Up, new CanMoveAndBeatEnemy()),
                new FieldAnalysisSequentialCaller(Direction.Down, new CanMoveAndBeatEnemy()),
                new FieldAnalysisSequentialCaller(Direction.Left, new CanMoveAndBeatEnemy()),
                new FieldAnalysisSequentialCaller(Direction.Right, new CanMoveAndBeatEnemy()),
            };
        }
    }
}