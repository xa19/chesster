﻿using System.Drawing;

namespace Szachy.Model.Pieces
{
    internal interface IDrawablePiece
    {
        string WhiteImgFilename { get; }
        string BlackImgFilename { get; }

        Bitmap WhiteImg { get; }
        Bitmap BlackImg { get; }
    }
}