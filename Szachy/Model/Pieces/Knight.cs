﻿using System.Collections.Generic;
using System.Drawing;
using Szachy.Model.SelectedPiecePossibilitiesAnalysis;
using Szachy.Model.SelectedPiecePossibilitiesAnalysis.FieldCheckingStrategy;

namespace Szachy.Model.Pieces
{
    public class Knight : Piece, IDrawablePiece
    {
        public string WhiteImgFilename { get; } = "white_knight.gif";
        public string BlackImgFilename { get; } = "black_knight.gif";
        public Bitmap WhiteImg { get; } = Properties.Resources.white_knight;
        public Bitmap BlackImg { get; } = Properties.Resources.black_knight;

        public Knight(TeamColor teamColor, Chessboard chessboard) : base(teamColor, chessboard)
        {
            SelectedPossibleFields = new List<FieldAnalysisCallerBase>
            {
                new FieldAnalysisSingularCaller(new CoordinateOffset(1, 2), new CanMoveAndBeatEnemy()),
                new FieldAnalysisSingularCaller(new CoordinateOffset(2, 1), new CanMoveAndBeatEnemy()),

                new FieldAnalysisSingularCaller(new CoordinateOffset(-1, 2), new CanMoveAndBeatEnemy()),
                new FieldAnalysisSingularCaller(new CoordinateOffset(-2, 1), new CanMoveAndBeatEnemy()),

                new FieldAnalysisSingularCaller(new CoordinateOffset(-1, -2), new CanMoveAndBeatEnemy()),
                new FieldAnalysisSingularCaller(new CoordinateOffset(-2, -1), new CanMoveAndBeatEnemy()),

                new FieldAnalysisSingularCaller(new CoordinateOffset(1, -2), new CanMoveAndBeatEnemy()),
                new FieldAnalysisSingularCaller(new CoordinateOffset(2, -1), new CanMoveAndBeatEnemy()),
            };
        }
    }
}