﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Szachy.Model.Pieces;

namespace Szachy.Model
{
    public class FieldsManager
    {
        public static int FieldsPerSide => Chessboard.StandardFieldsAmount;
        private readonly Field[,] _fields;

        public FieldsManager()
        {
            _fields = new Field[FieldsPerSide, FieldsPerSide];

            for (var row = 0; row < FieldsPerSide; row++)
            {
                for (var col = 0; col < FieldsPerSide; col++)
                {
                    _fields[row, col] = new Field(row, col);
                }
            }
        }

        public Field GetField(int row, int column)
        {
            return GetField(new CoordinateInChessboard(row, column));
        }

        public Field GetField(CoordinateInChessboard coords)
        {
            if (!coords.IsWithinChessboardRange())
            {
                throw new ArgumentException("Coordinate is pointing out of the chessboard");
            }

            Field field = _fields[coords.Row, coords.Column];

            if (field == null)
            {
                throw new NoNullAllowedException($"Field at [{coords.Row}, {coords.Column}] is null");
            }

            return field;
        }

        public IEnumerable<Field> GetAllFields()
        {
            for (int row = 0; row < FieldsPerSide; row++)
            {
                for (int col = 0; col < FieldsPerSide; col++)
                {
                    yield return GetField(row, col);
                }
            }
        }

        /// <returns>True if the field has been successfully deoccupied, otherwise there wasn't any occupying chess piece</returns>
        public bool DeoccupyFieldOnWhichThePieceIs(Piece piece)
        {
            CoordinateInChessboard coordForPiece = GetPositionOfPiece(piece);

            if (coordForPiece == null)
            {
                return false;
            }

            GetField(coordForPiece).Deoccupy();
            return true;
        }

        public bool ThereAreUninitializedFields()
        {
            return GetAllFields().Any(f => f == null);
        }

        // Must be run after calling fields analyzing method
        public uint GetAmountOfEnemyPiecesThatCanBeBeaten()
        {
            return (uint) GetAllFields().Count(f => f.Status.IsPossibleEnemyBeat());
        }

        public CoordinateInChessboard GetPositionOfPiece(Piece piece)
        {
            var fieldsWithThisPiece = GetAllFields().Where(x => x.IsOccupiedBy(piece)).ToArray();

            if (!fieldsWithThisPiece.Any())
            {
                return null;
            }

            if (fieldsWithThisPiece.Length == 1)
            {
                return fieldsWithThisPiece.First().Coords;
            }

            throw new Exception("There are at least 2 fields with the same chess piece!");
        }

        public Field GetFieldOnWhichThePieceIs(Piece piece)
        {
            CoordinateInChessboard pieceCoord = piece.GetCurrentPosition();
            return GetField(pieceCoord);
        }

        public void DisplayStatusArray()
        {
            Console.WriteLine("Chessboard:");

            // Upper chessboard edge
            for (uint row = 0; row < FieldsPerSide; row++)
            {
                Console.Write('-');
            }

            Console.WriteLine();

            for (uint row = 0; row < FieldsPerSide; row++)
            {
                for (uint col = 0; col < FieldsPerSide; col++)
                {
                    Field field = _fields[row, col];
                    if(field.Status.IsSelectedPiecePosition())
                    {
                        Console.Write('P');
                    }
                    else if(field.Status.IsPossibleEnemyBeat())
                    {
                        Console.Write('B');
                    }
                    else if (field.Status.IsPossibleMovement())
                    {
                        Console.Write('M');
                    }
                    else
                    {
                        Console.Write(' ');
                    }
                }

                // Right chessboard edge
                var rowMark = (FieldsPerSide - row).ToString();
                Console.WriteLine("|" + rowMark);
            }

            // Bottom chessboard edge
            for (uint row = 0; row < FieldsPerSide; row++)
                Console.Write('-');

            Console.WriteLine();

            // Column marks
            for (int row = 0; row < FieldsPerSide; row++)
            {
                var columnMark = CoordinateConverter.NumericIndexToLetter(row);
                Console.Write(columnMark);
            }

            Console.WriteLine();
        }
    }
}
