﻿using System;

namespace Szachy.Model
{
    public class Scenario
    {
        public int RequiredFieldsAmountPerSide { get; private set; }
        public Action PiecePlacements { private get; set; }

        public Scenario()
        {
            RequiredFieldsAmountPerSide = Chessboard.StandardFieldsAmount;
        }

        public void Apply()
        {
            PiecePlacements.Invoke();
        }
    }
}
