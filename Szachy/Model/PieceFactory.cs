﻿using System;
using System.Data;
using Szachy.Model.Pieces;

namespace Szachy.Model
{
    public static class PieceFactory
    {
        private static Chessboard _chessboard;

        public static TPiece Create<TPiece>(Piece.TeamColor color, CoordinateInChessboard startingPosition)
            where TPiece : Piece
        {
            if (_chessboard == null)
            {
                throw new NoNullAllowedException("Chessboard has not been initiated");
            }

            var piece = (TPiece)Activator.CreateInstance(typeof(TPiece), color, _chessboard);
            piece.PlaceOn(startingPosition, true);
            _chessboard.PieceManager.Pieces.Add(piece);

            return piece;
        }

        public static void SetChessboard(Chessboard chessBoard)
        {
            _chessboard = chessBoard;
            _chessboard.PieceManager.Pieces.Clear();
        }
    }
}