﻿using NUnit.Framework;
using System.Linq;
using Szachy.Model;
using Szachy.Model.Pieces;
using Szachy.Model.SelectedPiecePossibilitiesAnalysis;
using Szachy.ViewModel;

namespace SzachyTests
{
    [TestFixture]
    public class ChessboardTests
    {
        private Chessboard _chessboard;
        private Game _game;

        [SetUp]
        public void Init()
        {
            _game = new Game(initUi: false);
            _chessboard = _game.GetChessboard();
        }

        [Test]
        public void EveryFieldIsInitialized()
        {
            var numberOfNotInitializedFields = _chessboard.FieldManager.GetAllFields().Count(field => field == null);
            Assert.Zero(numberOfNotInitializedFields);
        }

        [Test]
        public void AfterChessboardInit_ThereAreTheSameNumberOfWhiteAndBlackPieces()
        {
            var numberOfWhitePieces = _chessboard.PieceManager.GetAllPieces().Count(x => x.IsWhite());
            var numberOfBlackPieces = _chessboard.PieceManager.GetAllPieces().Count(x => x.IsBlack());

            Assert.That(numberOfWhitePieces, Is.EqualTo(numberOfBlackPieces));
        }

        [Test]
        public void NumbersOfPieces_IsGreaterThanZero()
        {
            var numbersOfPieces = _chessboard.PieceManager.GetAllPieces().Count();
            Assert.That(numbersOfPieces, Is.GreaterThan(0));
        }

        [Test]
        public void AfterAnalyzingPieceMoves_PiecePositionShouldBeMarkedInAField()
        {
            ScenarioManager scenarioManager = new ScenarioManager(_chessboard);

            scenarioManager.ApplyScenario(new Scenario
            {
                PiecePlacements = delegate
                {
                    PieceFactory.Create<Castel>(Piece.TeamColor.White, new CoordinateInChessboard("C2"));
                }
            });

            Piece piece = _chessboard.PieceManager.GetAllPieces().First();

            FieldAnalysisCallerBase.AnalyzeSelectedPiecePossibilities(piece);

            Assert.That(_chessboard.FieldManager.GetFieldOnWhichThePieceIs(piece).Status.IsSelectedPiecePosition);
        }
    }
}