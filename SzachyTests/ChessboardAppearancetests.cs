﻿using NUnit.Framework;
using System;
using System.Windows;
using System.Windows.Media;
using Szachy.Model;
using Szachy.ViewModel.Visuals;

namespace SzachyTests
{
    [TestFixture]
    public class ChessboardAppearanceTests
    {
        private ChessboardVisuals _chessboardAppearance;

        [SetUp]
        public void Init()
        {
            _chessboardAppearance = new ChessboardVisuals(Colors.White, Colors.Black);
        }

        [Test]
        public void CheckerColors_AreNotNull()
        {
            Assert.That(_chessboardAppearance.FieldsColor1, Is.Not.Null);
        }

        [Test]
        public void FieldEdgeLength_IsGreaterThanZero_WhenChessboardEdgeLengthIsEqualTo1()
        {
            _chessboardAppearance.ChessboardEdgeLenghtInPx = 1;
            var fieldEdgeLength = _chessboardAppearance.GetFieldEdgeLength();

            Assert.That(fieldEdgeLength, Is.GreaterThan(0));
        }

        [Test]
        public void PositionInFormForField_WhenRowOrColumnBeyondRange_ThrowsException()
        {
            var fieldsPerSide = FieldsManager.FieldsPerSide;

            Assert.Throws<ArgumentOutOfRangeException>(delegate
            {
                Point positionForField = _chessboardAppearance.GetPositionForFieldAt(fieldsPerSide, fieldsPerSide);
            });
        }

        [Test]
        public void PositionInFormForField_ForFirstCorrectField_AreNonNegative()
        {
            Point positionForField = _chessboardAppearance.GetPositionForFieldAt(0, 0);

            Assert.That(positionForField.X, Is.GreaterThanOrEqualTo(0));
            Assert.That(positionForField.Y, Is.GreaterThanOrEqualTo(0));
        }

        [Test]
        public void PositionInFormForField_ForLastCorrectField_AreNonNegative()
        {
            var lastIndex = FieldsManager.FieldsPerSide - 1;

            Point positionForField = _chessboardAppearance.GetPositionForFieldAt(lastIndex, lastIndex);

            Assert.That(positionForField.X, Is.GreaterThanOrEqualTo(0));
            Assert.That(positionForField.Y, Is.GreaterThanOrEqualTo(0));
        }
    }
}