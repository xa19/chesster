﻿using NUnit.Framework;
using System;
using Szachy.Model;

namespace SzachyTests
{
    [TestFixture]
    public class CoordinatesTests
    {
        [Test]
        public void CorrectChessboardCoordinates_DoesntThrowExceptions()
        {
            Assert.DoesNotThrow(delegate
            {
                new CoordinateInChessboard("A1");
                new CoordinateInChessboard("A8");
                new CoordinateInChessboard("C3");
                new CoordinateInChessboard("H8");
            });
        }

        [Test]
        public void EmptyStringCoordinate_ThrowsExceptions()
        {
            Assert.Throws<ArgumentException>(delegate
            {
                new CoordinateInChessboard(String.Empty);
            });
        }

        [Test]
        public void OnlyLetterCoordinate_ThrowsExceptions()
        {
            Assert.Throws<ArgumentException>(delegate
            {
                new CoordinateInChessboard("A");
            });
        }

        [Test]
        public void OnlyNumberCoordinate_ThrowsExceptions()
        {
            Assert.Throws<ArgumentException>(delegate
            {
                new CoordinateInChessboard("3");
            });
        }

        [Test]
        public void OutOfRangeLetterCoordinate_ThrowsExceptions()
        {
            Assert.Throws<ArgumentException>(delegate
            {
                new CoordinateInChessboard("X1");
            });
        }

        [Test]
        public void OutOfRangeNumberCoordinate_ThrowsExceptions()
        {
            Assert.Throws<ArgumentException>(delegate
            {
                new CoordinateInChessboard("A9");
            });
        }

        [Test]
        public void TripleCharacterCoordinate_ThrowsExceptions()
        {
            Assert.Throws<ArgumentException>(delegate
            {
                new CoordinateInChessboard("AA9");
            });
            Assert.Throws<ArgumentException>(delegate
            {
                new CoordinateInChessboard("A99");
            });
        }

        [Test]
        public void ColumnIndexToLetter_WhenGiven0_ReturnsA()
        {
            char character = CoordinateConverter.NumericIndexToLetter(0);
            Assert.That(character, Is.EqualTo('A'));
        }

        [Test]
        public void ColumnIndexToLetter_WhenGiven1_ReturnsB()
        {
            char character = CoordinateConverter.NumericIndexToLetter(1);
            Assert.That(character, Is.EqualTo('B'));
        }

        [Test]
        public void ColumnIndexToLetter_WhenGiven7_ReturnsH()
        {
            char character = CoordinateConverter.NumericIndexToLetter(7);
            Assert.That(character, Is.EqualTo('H'));
        }

        [Test]
        public void CoordPlusZeroOffset_GivesTheSameCoord()
        {
            var coordBefore = new CoordinateInChessboard(1, 3);
            var coordAfter = coordBefore + new CoordinateOffset(0, 0);

            Assert.That(coordBefore.Row, Is.EqualTo(coordAfter.Row));
            Assert.That(coordBefore.Column, Is.EqualTo(coordAfter.Column));
        }
    }
}