﻿using System.Linq;
using NUnit.Framework;
using Szachy.Model;
using Szachy.Model.Pieces;
using Szachy.Model.SelectedPiecePossibilitiesAnalysis;

namespace SzachyTests
{
    [TestFixture]
    public class SituationsTests
    {
        private Chessboard _chessboard;
        private ScenarioManager _scenarioManager;
        private Piece.TeamColor _playerColor = Piece.TeamColor.White;
        private Piece.TeamColor _enemyColor = Piece.TeamColor.Black;

        [SetUp]
        public void Init()
        {
            _chessboard = new Chessboard();
            _scenarioManager = new ScenarioManager(_chessboard);
        }

        [Test]
        public void CastelSeesAllFourPawnBeatsVerticallyAndHorizontally()
        {
            _scenarioManager.ApplyScenario(new Scenario
            {
                PiecePlacements = delegate
                {
                    Create<Castel>(_playerColor, new CoordinateInChessboard("F5"));
                    Create<Pawn>(_enemyColor, new CoordinateInChessboard("F7"));
                    Create<Pawn>(_enemyColor, new CoordinateInChessboard("B5"));
                    Create<Pawn>(_enemyColor, new CoordinateInChessboard("F1"));
                    Create<Pawn>(_enemyColor, new CoordinateInChessboard("H5"));
                }
            });

            Castel piece = _chessboard.PieceManager.GetAllPiecesOfSpecifiedType<Castel>().First();
            FieldAnalysisCallerBase.AnalyzeSelectedPiecePossibilities(piece);

            Assert.That(_chessboard.FieldManager.GetAmountOfEnemyPiecesThatCanBeBeaten(), Is.EqualTo(4));
        }

        [Test]
        public void BishopSeesAllFourPawnBeatsAtDiagonal()
        {
            _scenarioManager.ApplyScenario(new Scenario
            {
                PiecePlacements = delegate
                {
                    Create<Bishop>(_playerColor, new CoordinateInChessboard("F5"));
                    Create<Pawn>(_enemyColor, new CoordinateInChessboard("D7"));
                    Create<Pawn>(_enemyColor, new CoordinateInChessboard("B1"));
                    Create<Pawn>(_enemyColor, new CoordinateInChessboard("G6"));
                    Create<Pawn>(_enemyColor, new CoordinateInChessboard("H3"));
                }
            });

            Bishop piece = _chessboard.PieceManager.GetAllPiecesOfSpecifiedType<Bishop>().First();
            FieldAnalysisCallerBase.AnalyzeSelectedPiecePossibilities(piece);

            Assert.That(_chessboard.FieldManager.GetAmountOfEnemyPiecesThatCanBeBeaten(), Is.EqualTo(4));
        }

        [Test]
        public void BishopSeesExactlyOnePawnToBeat()
        {
            _scenarioManager.ApplyScenario(new Scenario
            {
                PiecePlacements = delegate
                {
                    Create<Bishop>(_playerColor, new CoordinateInChessboard("F5"));
                    Create<Pawn>(_enemyColor, new CoordinateInChessboard("D7"));
                    Create<Pawn>(_enemyColor, new CoordinateInChessboard("A6"));
                    Create<Pawn>(_enemyColor, new CoordinateInChessboard("A2"));
                    Create<Pawn>(_enemyColor, new CoordinateInChessboard("F4"));
                }
            });

            Bishop piece = _chessboard.PieceManager.GetAllPiecesOfSpecifiedType<Bishop>().First();
            FieldAnalysisCallerBase.AnalyzeSelectedPiecePossibilities(piece);

            Assert.That(_chessboard.FieldManager.GetAmountOfEnemyPiecesThatCanBeBeaten(), Is.EqualTo(1));
        }

        [Test]
        public void CastelSeesExactlyTwoEnemyPawnsToBeat()
        {
            _scenarioManager.ApplyScenario(new Scenario
            {
                PiecePlacements = delegate
                {
                    Create<Castel>(_playerColor, new CoordinateInChessboard("F5"));
                    Create<Pawn>(_playerColor, new CoordinateInChessboard("F7"));
                    Create<Pawn>(_playerColor, new CoordinateInChessboard("B5"));
                    Create<Pawn>(_enemyColor, new CoordinateInChessboard("F1"));
                    Create<Pawn>(_enemyColor, new CoordinateInChessboard("H5"));
                }
            });

            Castel piece = _chessboard.PieceManager.GetAllPiecesOfSpecifiedType<Castel>().First();
            FieldAnalysisCallerBase.AnalyzeSelectedPiecePossibilities(piece);

            Assert.That(_chessboard.FieldManager.GetAmountOfEnemyPiecesThatCanBeBeaten(), Is.EqualTo(2));
        }

        [Test]
        public void TwoBishopsInRange_CanBeatEachOther()
        {
            _scenarioManager.ApplyScenario(new Scenario
            {
                PiecePlacements = delegate
                {
                    Create<Bishop>(_playerColor, new CoordinateInChessboard("C3"));
                    Create<Bishop>(_enemyColor, new CoordinateInChessboard("G7"));
                }
            });

            Piece playerPiece = _chessboard.PieceManager.GetAllPiecesOfSpecifiedColor(_playerColor).First();
            Piece enemyPiece = _chessboard.PieceManager.GetAllPiecesOfSpecifiedColor(_enemyColor).First();

            Assert.That(playerPiece.CanBeatPiece(enemyPiece));
        }

        private Piece _playerPiece;
        private Piece _enemyPiece;
        private void InitTwoOppositePieces(CoordinateInChessboard playerPos, CoordinateInChessboard enemyPos)
        {
            _scenarioManager.ApplyScenario(new Scenario
            {
                PiecePlacements = delegate
                {
                    Create<Pawn>(_playerColor, playerPos);
                    Create<Pawn>(_enemyColor, enemyPos);
                }
            });

            _playerPiece = _chessboard.PieceManager.GetAllPiecesOfSpecifiedColor(_playerColor).First();
            _enemyPiece = _chessboard.PieceManager.GetAllPiecesOfSpecifiedColor(_enemyColor).First();
        }

        [Test]
        public void TwoPawnsInFrontOfEachOther_CannotBeatEachOther()
        {
            InitTwoOppositePieces(new CoordinateInChessboard("E4"), new CoordinateInChessboard("E5"));
            Assert.That(!_playerPiece.CanBeatPiece(_enemyPiece));
            Assert.That(!_enemyPiece.CanBeatPiece(_playerPiece));
        }

        [Test]
        public void TwoPawnsInFrontOfEachOther_CannotMoveToEachOthersPosition()
        {
            InitTwoOppositePieces(new CoordinateInChessboard("E4"), new CoordinateInChessboard("E5"));
            Assert.That(!_playerPiece.CanBeatPiece(_enemyPiece));
        }

        [Test]
        public void TwoPawnsInFrontOfEachOther_WhenFirstMove_CannotHopOverEachOther()
        {
            InitTwoOppositePieces(new CoordinateInChessboard("E4"), new CoordinateInChessboard("E5"));

            FieldAnalysisCallerBase.AnalyzeSelectedPiecePossibilities(_playerPiece);
            var playerCanHopOverEnemy = _playerPiece.GetRelativeField(new CoordinateOffset(2, 0)).Status.IsPossibleMovement();
            Assert.That(!playerCanHopOverEnemy);

            FieldAnalysisCallerBase.AnalyzeSelectedPiecePossibilities(_enemyPiece);
            var enemyCanHopOverPlayer = _enemyPiece.GetRelativeField(new CoordinateOffset(2, 0)).Status.IsPossibleMovement();
            Assert.That(!enemyCanHopOverPlayer);
        }

        [Test]
        public void TwoNearbyPawnsInDiagonal_CanBeatEachOther()
        {
            InitTwoOppositePieces(new CoordinateInChessboard("E4"), new CoordinateInChessboard("D5"));

            Assert.That(_playerPiece.CanBeatPiece(_enemyPiece));
            Assert.That(_enemyPiece.CanBeatPiece(_playerPiece));
        }

        [Test]
        public void TwoNearbyPawnsInDiagonal_CanMoveOneOrTwoStepsAheadIfFirstMove()
        {
            InitTwoOppositePieces(new CoordinateInChessboard("E4"), new CoordinateInChessboard("D5"));

            FieldAnalysisCallerBase.AnalyzeSelectedPiecePossibilities(_playerPiece);
            var playerPossibleMoveOneUp = _playerPiece.GetRelativeField(new CoordinateOffset(1, 0)).Status.IsPossibleMovement();
            var playerPossibleMoveTwoUp = _playerPiece.GetRelativeField(new CoordinateOffset(2, 0)).Status.IsPossibleMovement();

            FieldAnalysisCallerBase.AnalyzeSelectedPiecePossibilities(_enemyPiece);
            var enemyPossibleMoveOneUp = _enemyPiece.GetRelativeField(new CoordinateOffset(-1, 0)).Status.IsPossibleMovement();
            var enemyPossibleMoveTwoUp = _enemyPiece.GetRelativeField(new CoordinateOffset(-2, 0)).Status.IsPossibleMovement();

            Assert.That(playerPossibleMoveOneUp);
            Assert.That(playerPossibleMoveTwoUp);
            Assert.That(enemyPossibleMoveOneUp);
            Assert.That(enemyPossibleMoveTwoUp);
        }

        [Test]
        public void TwoNearbyPawnsInDiagonal_CanMoveOnlyOneSteapAheadIfNOTFirstMove()
        {
            InitTwoOppositePieces(new CoordinateInChessboard("E4"), new CoordinateInChessboard("D5"));

            _playerPiece.SetMovesCounter(1);
            _enemyPiece.SetMovesCounter(1);

            FieldAnalysisCallerBase.AnalyzeSelectedPiecePossibilities(_playerPiece);
            var playerPossibleMoveOneUp = _playerPiece.GetRelativeField(new CoordinateOffset(1, 0)).Status.IsPossibleMovement();

            FieldAnalysisCallerBase.AnalyzeSelectedPiecePossibilities(_enemyPiece);
            var enemyPossibleMoveOneUp = _enemyPiece.GetRelativeField(new CoordinateOffset(-1, 0)).Status.IsPossibleMovement();

            Assert.That(playerPossibleMoveOneUp);
            Assert.That(enemyPossibleMoveOneUp);
        }

        [Test]
        public void PawnCannotMoveIntoDiagonal()
        {
            _scenarioManager.ApplyScenario(new Scenario
            {
                PiecePlacements = delegate
                {
                    Create<Pawn>(_playerColor, new CoordinateInChessboard("E4"));
                }
            });

            Piece playerPiece = _chessboard.PieceManager.GetAllPiecesOfSpecifiedColor(_playerColor).First();

            FieldAnalysisCallerBase.AnalyzeSelectedPiecePossibilities(playerPiece);
            Field diagonalField1 = playerPiece.GetRelativeField(new CoordinateOffset(1, -1));
            Field diagonalField2 = playerPiece.GetRelativeField(new CoordinateOffset(1, 1));

            Assert.That(!diagonalField1.Status.IsPossibleMovement() && !diagonalField2.Status.IsPossibleMovement());
        }

        private void Create<TPiece>(Piece.TeamColor color, CoordinateInChessboard startingPosition)
            where TPiece : Piece
        {
            PieceFactory.Create<TPiece>(color, startingPosition);
        }
    }
}