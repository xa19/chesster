﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using NUnit.Framework;
using Szachy.Model;
using Szachy.Model.Pieces;
using Szachy.Model.SelectedPiecePossibilitiesAnalysis;

namespace SzachyTests
{
    [TestFixture]
    public class PiecesTests
    {
        private readonly Chessboard _chessboard = new Chessboard();
        private Piece _blackKing;
        private readonly Piece.TeamColor _white = Piece.TeamColor.White;

        [SetUp]
        public void Init()
        {
            PieceManager.DeoccupyAllFields(_chessboard);
        }

        [Test]
        public void PlacingPieceIntoNewPosition_TakesEffect()
        {
            _blackKing = new King(Piece.TeamColor.Black, _chessboard);

            _blackKing.PlaceOn(new CoordinateInChessboard(3, 3), true);
            var currentPosition = _blackKing.GetCurrentPosition();

            _blackKing.PlaceOn(new CoordinateInChessboard(2, 1), true);
            var newPosition = _blackKing.GetCurrentPosition();

            Assert.That(currentPosition.Row, Is.Not.EqualTo(newPosition.Row));
            Assert.That(currentPosition.Column, Is.Not.EqualTo(newPosition.Column));
        }

        private Field GetRelativeFieldForPawn(CoordinateOffset offset, int piecePosX, int piecePosY)
        {
            Pawn piece = PieceFactory.Create<Pawn>(_white, new CoordinateInChessboard(piecePosX, piecePosY));
            var fieldChecker = new FieldAnalysisSingularCaller(offset);
            List<Field> analyzedFields = fieldChecker.AnalyzeField(piece).ToList();

            if(!analyzedFields.Any())
            {
                return null;
            }

            return analyzedFields.First();
        }

        private Field GetRelativeFieldForPawnAtTopLeft(CoordinateOffset offset)
        {
            return GetRelativeFieldForPawn(offset, 0, 0);
        }

        private Field GetRelativeFieldForPawnAtBottomRight(CoordinateOffset offset)
        {
            int limitIndex = Chessboard.StandardFieldsAmount - 1;
            return GetRelativeFieldForPawn(offset, limitIndex, limitIndex);
        }

        [Test]
        public void WhenGettingRelativeFieldOutOfChessboard_CaseLeftUp_StatusIsIncorrectCoord()
        {
            Field field = GetRelativeFieldForPawnAtTopLeft(new CoordinateOffset(-1, -1));

            Assert.That(field, Is.Null);
        }

        [Test]
        public void WhenGettingRelativeFieldOutOfChessboard_CaseLeft_StatusIsIncorrectCoord()
        {
            Field field = GetRelativeFieldForPawnAtTopLeft(new CoordinateOffset(0, -1));
            Assert.That(field, Is.Null);
        }

        [Test]
        public void WhenGettingRelativeFieldOutOfChessboard_CaseRightDown_StatusIsIncorrectCoord()
        {
            Field field = GetRelativeFieldForPawnAtBottomRight(new CoordinateOffset(1, 1));
            Assert.That(field, Is.Null);
        }

        [Test]
        public void WhenGettingRelativeFieldOutOfChessboard_CaseRight_StatusIsIncorrectCoord()
        {
            Field field = GetRelativeFieldForPawnAtBottomRight(new CoordinateOffset(0, 1));
            Assert.That(field, Is.Null);
        }      
    }
}